# eventando

The Event Registration System is a project developed in PHP, HTML, CSS, JavaScript, MySQL, and Bootstrap. Its purpose is to facilitate the management and registration of events in a web application.

## Demo

![Eventando Demo](https://gitlab.com/ufv-projects/eventando/-/raw/main/img/demo.png)

## Prerequisites

Ensure that you have the following programs installed on your machine:

1. **XAMPP**: XAMPP is a package that includes the Apache web server, MySQL database, PHP script language interpreter, and other components needed to run the project. You can download XAMPP from the [Apache Friends](https://www.apachefriends.org/) website and follow the appropriate installation instructions for your operating system.

2. **Composer**: Composer is a dependency management tool for PHP. You can download and install Composer from the [Get Composer](https://getcomposer.org/) website. Follow the appropriate installation instructions for your operating system.

## Installation and Development

### Step 1: Clone the repository

After installing XAMPP, access the installation folder and locate the "htdocs" folder. In this folder, you should clone the project repository using Git or by downloading the source code as a ZIP file and extracting it. Ensure that the repository is in a folder named "eventando" within the "htdocs" folder.

```
cd /path/to/xampp/htdocs
git clone https://gitlab.com/ufv-projects/eventando.git
```

Also, remember to give write, read, and execute permissions to the "temp" folder in XAMPP to ensure the correct operation of the project.

### Step 2: Install dependencies

After cloning the repository, navigate to the project directory and install the dependencies using Composer. The dependencies will be specified in the `composer.json` file, and the `composer install` command will download and install them automatically.

```
cd /path/to/xampp/htdocs/eventando
composer install
```

### Step 3: Configure the database

Create a MySQL database named 'EventandoDB' on your local server. This can be done using a MySQL client such as PhpMyAdmin or the command line.

After creating the database, create a file named `.env` at the root of the project and fill in the database information in it, using the `.env.sample` file provided in the project as a base:

```ini
[MySQL]
database="EventandoDB"
user="root"
password="root"
host="localhost:3306"
```

### Step 4: Run scripts to create tables and fake data

The project contains scripts that are responsible for creating database tables and filling in some initial information (fake data or seeds). Navigate to the "scripts" folder and copy and paste them into your MySQL client such as PhpMyAdmin or the command line.

After following these steps, the project will be configured and ready to run. You can access it in your browser using [localhost](http://localhost/eventando).

## Contributing

If you want to contribute to this project, feel free to open a pull request. All contributions are welcome!

## License

This project is licensed under the [MIT License](https://gitlab.com/ufv-projects/eventando/-/blob/main/LICENSE). See the LICENSE file for more details.
