<?php

class Core
{
  private $url;
  private $controller;
  private $method = "index";
  private $params = [];
  private $user;
  private $error;

  public function __construct()
  {
    $this->user = $_SESSION["user"] ?? null;
    $this->error = $_SESSION["message_error"] ?? null;

    $this->handleErrorMessage();
  }

  private function handleErrorMessage()
  {
    if (isset($this->error)) {
      if ($this->error["count"] === 0) {
        $_SESSION["message_error"]["count"]++;
      } else {
        unset($_SESSION["message_error"]);
      }
    }
  }

  public function start($request)
  {
    $this->parseUrl($request["url"]);
    $this->validateAndSetController();
    return $this->executeControllerMethod();
  }

  private function parseUrl($url)
  {
    if (isset($url)) {
      $this->url = explode("/", $url);
      $this->controller = ucfirst($this->url[0]) . "Controller";
      array_shift($this->url);

      if (!empty($this->url)) {
        $this->method = $this->url[0];
        array_shift($this->url);

        if (!empty($this->url)) {
          $this->params = $this->url;
        }
      }
    }
  }

  private function validateAndSetController()
  {
    $allowedControllers = $this->getAllowedControllers();

    if (!isset($this->controller) || !in_array($this->controller, $allowedControllers)) {
      $this->controller = $this->user ? "DashboardController" : "LoginController";
      $this->method = "index";
    }
  }

  private function getAllowedControllers()
  {
    $pagesPermission = ["LoginController", "RegisterController"];

    if ($this->user) {
      $pagesPermission = array_merge($pagesPermission, ["DashboardController", "EventController", "UserController", "ReviewController"]);

      if ($this->user['user_type'] === 'administrator') {
        $pagesPermission[] = "AdminController";
      }
    }

    return $pagesPermission;
  }

  private function executeControllerMethod()
  {
    return call_user_func([new $this->controller, $this->method], $this->params);
  }
}
