<?php

use \Luiz\Database\Connection;

abstract class CategoryModel
{
  public static function getAllCategories()
  {
    $connection = Connection::get();
    $sql = "SELECT name FROM category";
    $stmt = $connection->prepare($sql);
    $stmt->execute();
    return $stmt->fetchAll();
  }

  public static function getAllCategoriesByPage($page)
  {
    $connection = Connection::get();
    $offset = $page * 3;
    $sql = "SELECT * FROM category LIMIT 3 OFFSET :offset";
    $stmt = $connection->prepare($sql);
    $stmt->bindValue(':offset', $offset, PDO::PARAM_INT);
    $stmt->execute();
    $categories = $stmt->fetchAll();
    return $categories;
  }

  public static function getCategoryId($categoryName)
  {
    $connection = Connection::get();
    $sql = "SELECT category_id FROM category WHERE name = :categoryName";
    $stmt = $connection->prepare($sql);
    $stmt->bindParam(':categoryName', $categoryName, PDO::PARAM_STR);
    $stmt->execute();
    return $stmt->fetch()["category_id"];
  }

  public static function getNumCategories()
  {
    $connection = Connection::get();
    $sql = "SELECT count(*) AS total FROM category";
    $stmt = $connection->prepare($sql);
    $stmt->execute();
    $result = $stmt->fetch();
    return $result["total"];
  }

  public static function delete($categoryId)
  {
    $connection = Connection::get();
    $count = self::countEventsInCategory($connection, $categoryId);

    if ($count > 0) {
      self::updateEventCategory($connection, $categoryId);
    }

    self::removeCategory($connection, $categoryId);
  }

  private static function countEventsInCategory($connection, $categoryId)
  {
    $sql = "SELECT COUNT(*) FROM event WHERE category_id = :categoryId";
    $stmt = $connection->prepare($sql);
    $stmt->bindParam(':categoryId', $categoryId, PDO::PARAM_INT);
    $stmt->execute();
    return $stmt->fetchColumn();
  }

  private static function updateEventCategory($connection, $categoryId)
  {
    $sql = "UPDATE event SET category_id = 1 WHERE category_id = :categoryId";
    $stmt = $connection->prepare($sql);
    $stmt->bindParam(':categoryId', $categoryId, PDO::PARAM_INT);
    $stmt->execute();
  }

  private static function removeCategory($connection, $categoryId)
  {
    $sql = "DELETE FROM category WHERE category_id = :categoryId";
    $stmt = $connection->prepare($sql);
    $stmt->bindParam(':categoryId', $categoryId, PDO::PARAM_INT);
    $stmt->execute();
  }
}
