<?php

use \Luiz\Database\Connection;

abstract class EventModel
{
  public static function getAllEventsByPage($page)
  {
    $connection = Connection::get();
    $offset = $page * 3;
    $sql = "SELECT * FROM event LIMIT 3 OFFSET :offset";
    $stmt = $connection->prepare($sql);
    $stmt->bindValue(':offset', $offset, PDO::PARAM_INT);
    $stmt->execute();
    $events = $stmt->fetchAll();

    foreach ($events as &$event) {
      $event["images"] = explode(",", $event["images"]);
    }

    return $events;
  }

  public static function getNumOfEvents()
  {
    $connection = Connection::get();
    $sql = "SELECT count(*) AS total FROM event";
    $stmt = $connection->prepare($sql);
    $stmt->execute();
    $result = $stmt->fetch();
    return $result["total"];
  }

  public static function getSpecificsEventsByPageAndCategory($title, $page, $categoryName)
  {
    $connection = Connection::get();
    $offset = $page * 3;

    $sql = "SELECT event_id FROM event WHERE title LIKE :title_pattern ";

    if ($categoryName !== 'Todas') {
      $categoryId = CategoryModel::getCategoryId($categoryName);
      $sql .= "AND category_id = :category_id ";
    }

    $sql .= "LIMIT 3 OFFSET :offset";

    $stmt = $connection->prepare($sql);
    $stmt->bindValue(':title_pattern', '%' . $title . '%', PDO::PARAM_STR);
    $stmt->bindValue(':offset', $offset, PDO::PARAM_INT);

    if ($categoryName !== 'Todas') {
      $stmt->bindValue(':category_id', $categoryId, PDO::PARAM_INT);
    }

    $stmt->execute();
    $eventsId = $stmt->fetchAll();
    $events = self::fetchEventsByIds($connection, $eventsId);

    return $events;
  }

  private static function fetchEventsByIds($connection, $eventsId)
  {
    $events = [];

    foreach ($eventsId as $eventId) {
      $sql = "SELECT * FROM event WHERE event_id = :event_id";
      $stmt = $connection->prepare($sql);
      $stmt->bindValue(':event_id', $eventId["event_id"], PDO::PARAM_INT);
      $stmt->execute();
      $event = $stmt->fetch();
      $event["images"] = explode(",", $event["images"]);
      $events[] = $event;
    }

    return $events;
  }

  public static function getEventsRegistrated($userId)
  {
    $connection = Connection::get();
    $sql = "SELECT event_id FROM registration WHERE user_id = :user_id";
    $stmt = $connection->prepare($sql);
    $stmt->bindValue(":user_id", $userId, PDO::PARAM_INT);
    $stmt->execute();
    return $stmt->fetchAll();
  }

  public static function getEventsByPage($eventsId, $page)
  {
    if (empty($eventsId)) {
      return [];
    }

    $connection = Connection::get();
    $offset = $page * 3;
    $limit = 3;

    $eventIdsString = implode(',', array_column($eventsId, 'event_id'));

    $sql = "SELECT * FROM event WHERE event_id IN ({$eventIdsString}) ORDER BY event_id LIMIT :limit OFFSET :offset";
    $stmt = $connection->prepare($sql);
    $stmt->bindValue(':limit', $limit, PDO::PARAM_INT);
    $stmt->bindValue(':offset', $offset, PDO::PARAM_INT);
    $stmt->execute();
    $events = $stmt->fetchAll();

    foreach ($events as &$event) {
      $event["images"] = explode(",", $event["images"]);
    }

    return $events;
  }

  public static function getEventsOrganizer($userId)
  {
    $connection = Connection::get();
    $sql = "SELECT event_id FROM event WHERE user_id = :user_id";
    $stmt = $connection->prepare($sql);
    $stmt->bindValue(":user_id", $userId, PDO::PARAM_INT);
    $stmt->execute();
    return $stmt->fetchAll();
  }

  public static function insert($title, $price, $date, $time, $locale, $category, $image, $description, $userId)
  {
    $connection = Connection::get();
    $sql = "INSERT INTO event (title, description, date, time, location, category_id, user_id, price, images) VALUES (:title, :description, :date, :time, :location, :category_id, :user_id, :price, :images)";
    $stmt = $connection->prepare($sql);
    $stmt->bindValue(":title", $title, PDO::PARAM_STR);
    $stmt->bindValue(":price", $price, PDO::PARAM_STR);
    $stmt->bindValue(":date", $date, PDO::PARAM_STR);
    $stmt->bindValue(":time", $time, PDO::PARAM_STR);
    $stmt->bindValue(":location", $locale, PDO::PARAM_STR);
    $stmt->bindValue(":category_id", $category, PDO::PARAM_INT);
    $stmt->bindValue(":images", $image, PDO::PARAM_STR);
    $stmt->bindValue(":description", $description, PDO::PARAM_STR);
    $stmt->bindValue(":user_id", $userId, PDO::PARAM_INT);

    if ($stmt->execute()) {
      return $connection->lastInsertId();
    } else {
      return false;
    }
  }

  public static function getEventsByUser($userId)
  {
    $connection = Connection::get();
    $sql = "SELECT event_id FROM event WHERE user_id = :user_id";
    $stmt = $connection->prepare($sql);
    $stmt->bindValue(":user_id", $userId, PDO::PARAM_INT);
    $stmt->execute();
    return $stmt->fetchAll();
  }

  public static function delete($id)
  {
    $connection = Connection::get();
    $deleteRegistrationSql = "DELETE FROM registration WHERE event_id = :event_id";
    $deleteReviewSql = "DELETE FROM review WHERE event_id = :event_id";
    $stmtRegistration = $connection->prepare($deleteRegistrationSql);
    $stmtReview = $connection->prepare($deleteReviewSql);
    $stmtRegistration->bindValue(":event_id", $id, PDO::PARAM_INT);
    $stmtReview->bindValue(":event_id", $id, PDO::PARAM_INT);
    $stmtRegistration->execute();
    $stmtReview->execute();
    $deleteEventSql = "DELETE FROM event WHERE event_id = :event_id";
    $stmtEvent = $connection->prepare($deleteEventSql);
    $stmtEvent->bindValue(":event_id", $id, PDO::PARAM_INT);
    return $stmtEvent->execute();
  }

  public static function getEvent($id)
  {
    $connection = Connection::get();
    $selectEventSql = "SELECT * FROM event WHERE event_id = :event_id";
    $stmt = $connection->prepare($selectEventSql);
    $stmt->bindValue(":event_id", $id, PDO::PARAM_INT);
    $stmt->execute();
    $event = $stmt->fetch();
    $event["images"] = explode(",", $event["images"]);
    return $event;
  }
}
