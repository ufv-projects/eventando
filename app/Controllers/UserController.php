<?php

class UserController extends BaseController
{
  public function index($args)
  {
    $loader = new \Twig\Loader\FilesystemLoader("app/Views");
    $twig = new \Twig\Environment($loader, ["auto_reload" => true]);
    $template = $twig->load("user.html");

    $userId = intval($args[0]);
    $user = UserModel::getUser($userId);

    $categories = CategoryModel::getAllCategories();
    $loggedInUser = $_SESSION["user"] ?? null;

    if ($user["user_type"] === 'participant') {
      $eventsId = EventModel::getEventsRegistrated($userId);
    } else if ($user["user_type"] === 'organizer') {
      $eventsId = EventModel::getEventsOrganizer($userId);
    } else {
      $eventsId = [];
    }

    $pageToLoad = isset($args[1]) ? intval($args[1]) : 0;
    $eventsPerPage = 3;
    $events = EventModel::getEventsByPage($eventsId, $pageToLoad);

    $totalNumEvents = ceil(count($eventsId) / $eventsPerPage) - 1;

    $parameters = [
      "user" => $user,
      "categories" => $categories,
      "loggedInUser" => $loggedInUser,
      "events" => $events,
      "totalNumEvents" => $totalNumEvents,
      "currentPage" => $pageToLoad
    ];

    return $template->render($parameters);
  }

  public function logout()
  {
    session_unset();
    session_destroy();
    $this->redirectTo("/eventando/");
  }
}
