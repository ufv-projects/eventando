<?php

class ReviewController
{
  public function create($params)
  {
    $eventId = intval($params[0]);
    $comment = $_POST["comment"] ?? '';
    $userId = $_SESSION["user"]["user_id"] ?? null;
    $score = $_POST["score"] ?? 0;

    if ($userId !== null) {
      ReviewModel::insert($userId, $eventId, $score, $comment);
    }

    $redirectTo = "/eventando/event/index/{$eventId}";
    header("Location: $redirectTo");
  }
}
