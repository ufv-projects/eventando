<?php

use Luiz\Database\Connection;

class RegisterController extends BaseController
{
  public function index()
  {
    $parameters["error"] = $_SESSION["message_error"] ?? null;
    $template = $this->twig->load("register.html");
    return $template->render($parameters);
  }

  public function create()
  {
    try {
      $formFields = ["password", "confirmPassword"];
      $this->validateFormData($_POST, $formFields);

      $user = $_POST["name"];
      $email = $_POST["email"];
      $password = $_POST["password"];
      $userType = $_POST["userType"];

      UserModel::insert($user, $email, $password, $userType);
      $this->redirectTo("/eventando/dashboard");
    } catch (Exception $e) {
      $_SESSION["message_error"] = ["message" => $e->getMessage(), "count" => 0];
      $this->redirectTo("/eventando/register");
    }
  }
}
