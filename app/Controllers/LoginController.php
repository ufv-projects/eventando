<?php

class LoginController extends BaseController
{
  public function index()
  {
    $parameters["error"] = $_SESSION["message_error"] ?? null;
    $template = $this->twig->load("login.html");
    return $template->render($parameters);
  }

  public function check()
  {
    try {
      $formFields = ["email", "password"];
      $this->validateFormData($_POST, $formFields);

      $email = $_POST["email"];
      $password = $_POST["password"];

      UserModel::validateLogin($email, $password);
      $this->redirectTo("/eventando/dashboard");
    } catch (Exception $e) {
      $_SESSION["message_error"] = ["message" => $e->getMessage(), "count" => 0];
      $this->redirectTo("/eventando/login");
    }
  }
}
