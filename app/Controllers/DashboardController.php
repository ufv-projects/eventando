<?php

class DashboardController extends BaseController
{
  public function index($page)
  {
    $pageToLoad = isset($page[0]) ? intval($page[0]) : 0;

    $events = EventModel::getAllEventsByPage($pageToLoad);
    $totalNumEvents = ceil(EventModel::getNumOfEvents() / 3) - 1;
    $categories = CategoryModel::getAllCategories();
    $loggedInUser = $_SESSION["user"] ?? null;

    $parameters = [
      "events" => $events,
      "total_num_events" => $totalNumEvents,
      "categories" => $categories,
      "loggedInUser" => $loggedInUser,
      "current_page" => $pageToLoad
    ];

    $template = $this->twig->load("dashboard.html");
    return $template->render($parameters);
  }
}
