<?php

class BaseController
{
  protected $twig;

  public function __construct()
  {
    $loader = new \Twig\Loader\FilesystemLoader("app/Views");
    $this->twig = new \Twig\Environment($loader, ["auto_reload" => true]);
  }

  protected function render($templateFile, $parameters = [])
  {
    $parameters["loggedInUser"] = $_SESSION["user"] ?? null;

    $template = $this->twig->load($templateFile);
    return $template->render($parameters);
  }

  protected function redirectTo($location)
  {
    header("Location: $location");
    exit;
  }

  protected function validateFormData($formData, $fields)
  {
    foreach ($fields as $field) {
      if (!isset($formData[$field])) {
        throw new Exception("Preencha todos os campos corretamente.");
      }
    }
  }

  protected function getPageNumber($param)
  {
    return isset($param[0]) ? intval($param[0]) : 0;
  }
}
