<?php

class EventController extends BaseController
{
  public function index($params)
  {
    $eventId = intval($params[0]);
    $pageToLoad = isset($params[1]) ? intval($params[1]) : 0;

    $event = EventModel::getEvent($eventId);
    $reviews = ReviewModel::getAllReviewsByPageEventId($pageToLoad, $eventId);
    $totalNumReviews = ceil(ReviewModel::getNumReviewsByEvent($eventId) / 3) - 1;

    $categories = CategoryModel::getAllCategories();
    $loggedInUser = $_SESSION["user"] ?? null;

    $parameters = [
      "event" => $event,
      "reviews" => $reviews,
      "total_num_reviews" => $totalNumReviews,
      "current_page" => $pageToLoad,
      "categories" => $categories,
      "loggedInUser" => $loggedInUser
    ];

    $template = $this->twig->load("event.html");
    return $template->render($parameters);
  }

  public function list($page)
  {
    $pageToLoad = isset($page[0]) ? intval($page[0]) : 0;
    $searchName = $_POST["name"] ?? '';
    $categoryName = $_POST["category"] ?? '';

    $searchedEvents = EventModel::getSpecificsEventsByPageAndCategory($searchName, $pageToLoad, $categoryName);
    $categories = CategoryModel::getAllCategories();
    $totalNumEvents = ceil(count($searchedEvents) / 3) - 1;
    $loggedInUser = $_SESSION["user"] ?? null;

    $parameters = [
      "events" => $searchedEvents,
      "total_num_events" => $totalNumEvents,
      "category" => $categoryName,
      "search_name" => $searchName,
      "categories" => $categories,
      "loggedInUser" => $loggedInUser
    ];

    $template = $this->twig->load("event_list.html");
    return $template->render($parameters);
  }

  public function create()
  {
    $title = $_POST["title"];
    $price = substr($_POST["price"], 1);
    $date = $_POST["date"];
    $time = $_POST["time"];
    $locale = $_POST["locale"];
    $categoryId = CategoryModel::getCategoryId($_POST["category"]);
    $description = $_POST["description"];
    $userId = $_SESSION["user"]["user_id"];

    $images = [];
    if (isset($_FILES["image"])) {
      $imageFolder = "uploads/";

      foreach ($_FILES["image"]["tmp_name"] as $key => $tmpName) {
        $imageName = $_FILES["image"]["name"][$key];
        $imagePath = $imageFolder . $imageName;

        if (move_uploaded_file($tmpName, $imagePath)) {
          $images[] = $imagePath;
        }
      }
    }

    $imagesStr = implode(",", $images);
    $eventInsertedId = EventModel::insert($title, $price, $date, $time, $locale, $categoryId, $imagesStr, $description, $userId);

    if ($eventInsertedId) {
      $this->redirectTo("/eventando/event/index/{$eventInsertedId}");
    }
  }
}
