<?php

class AdminController extends BaseController
{
  public function index($param)
  {
    $this->redirectTo("/eventando/admin/event/0");
  }

  public function event($param)
  {
    return $this->renderAdminDashboard("admin_dashboard_event.html", "events", EventModel::getNumOfEvents(), 'EventModel::getAllEventsByPage', $param);
  }

  public function user($param)
  {
    return $this->renderAdminDashboard("admin_dashboard_user.html", "users", UserModel::getNumOfUsers(), 'UserModel::getAllUsersByPage', $param);
  }

  public function review($param)
  {
    return $this->renderAdminDashboard("admin_dashboard_review.html", "reviews", ReviewModel::getNumReviews(), 'ReviewModel::getAllReviewsByPage', $param);
  }

  public function category($param)
  {
    return $this->renderAdminDashboard("admin_dashboard_category.html", "categories_dashboard", CategoryModel::getNumCategories(), 'CategoryModel::getAllCategoriesByPage', $param);
  }

  public function delete($params)
  {
    $tableName = $params[0];
    $id = $params[1];

    switch ($tableName) {
      case 'event':
        EventModel::delete($id);
        $this->redirectTo("/eventando/admin/event/0");
        break;
      case 'user':
        UserModel::delete($id);
        $this->redirectTo("/eventando/admin/user/0");
        break;
      case 'review':
        ReviewModel::delete($id);
        $this->redirectTo("/eventando/admin/review/0");
        break;
      case 'category':
        CategoryModel::delete($id);
        $this->redirectTo("/eventando/admin/category/0");
        break;
      default:
        $this->redirectTo("/eventando/admin/event/0");
        break;
    }
  }

  private function renderAdminDashboard($templateFile, $parameterName, $numOfItems, $fetchItemsFunction, $param)
  {
    $pageToLoad = isset($param[0]) ? intval($param[0]) : 0;

    $items = call_user_func($fetchItemsFunction, $pageToLoad);
    $categories = CategoryModel::getAllCategories();
    $loggedInUser = $_SESSION["user"] ?? null;

    $parameters = [
      $parameterName => $items,
      "total_num_$parameterName" => ceil($numOfItems / 3) - 1,
      "categories" => $categories,
      "loggedInUser" => $loggedInUser,
      "current_page" => $pageToLoad
    ];

    $template = $this->twig->load($templateFile);
    return $template->render($parameters);
  }
}
